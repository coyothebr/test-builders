package com.willalves.test.controller;

import com.querydsl.core.BooleanBuilder;
import com.willalves.test.assembler.UserAssembler;
import com.willalves.test.domain.User;
import com.willalves.test.dto.UserDTO;
import com.willalves.test.dto.UserPageableDTO;
import com.willalves.test.repository.specification.CustomUserSpecification;
import com.willalves.test.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@AllArgsConstructor
@Controller
public class UserController {

  private final UserAssembler userAssembler;
  private final PagedResourcesAssembler<User> pagedResourcesAssembler;
  private final UserService userService;

  @PostMapping(value = "/user", consumes = "application/json", produces = "application/json")
  public ResponseEntity<User> create(@Valid @RequestBody UserDTO user) {
    return ResponseEntity.status(HttpStatus.CREATED).body(userService.save(user.toDomain()));
  }

  @PutMapping(value = "/user/{id}", consumes = "application/json", produces = "application/json")
  public ResponseEntity<?> put(@Valid @PathVariable String id, @Valid @RequestBody UserDTO user) {

    return ResponseEntity.status(HttpStatus.CREATED).body(userService.putUpdate(id, user));
  }

  @PatchMapping(value = "/user/{id}", consumes = "application/json", produces = "application/json")
  public ResponseEntity<?> patch(@Valid @PathVariable String id, @Valid @RequestBody UserDTO user) {

    return ResponseEntity.status(HttpStatus.ACCEPTED).body(userService.patchUpdate(id, user));
  }

  @DeleteMapping(value = "/user/{id}")
  public ResponseEntity<?> remove(@Valid @PathVariable String id) {
    userService.delete(id);
    return ResponseEntity.status(HttpStatus.ACCEPTED).body("");
  }

  @GetMapping("/user/{id}")
  public ResponseEntity<User> getOrderById(@PathVariable String id) {
    User user = userService.getById(id);
    return ResponseEntity.status(HttpStatus.OK).body(user);
  }

  public ResponseEntity<CollectionModel<UserPageableDTO>> getAll() {
    List<User> users = userService.findAll();

    return new ResponseEntity<>(userAssembler.toCollectionModel(users), HttpStatus.OK);
  }

  @GetMapping("/user")
  public ResponseEntity<PagedModel<UserPageableDTO>> getAll(
      Pageable pageable,
      @RequestParam(name = "name", required = false) String name,
      @RequestParam(name = "cpf", required = false) String cpf) {
    BooleanBuilder booleanBuilder = new BooleanBuilder();

    if (name != null && !name.isEmpty()) {
      booleanBuilder.and(CustomUserSpecification.name(name));
    }

    if (cpf != null && !cpf.isEmpty()) {
      booleanBuilder.and(CustomUserSpecification.cpf(cpf));
    }

    Page<User> orderEntities = userService.findAll(pageable, booleanBuilder);

    PagedModel<UserPageableDTO> collModel =
        pagedResourcesAssembler.toModel(orderEntities, userAssembler);

    return new ResponseEntity<>(collModel, HttpStatus.OK);
  }
}
