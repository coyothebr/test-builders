package com.willalves.test.assembler;

import com.willalves.test.controller.UserController;
import com.willalves.test.domain.User;
import com.willalves.test.dto.UserPageableDTO;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class UserAssembler extends RepresentationModelAssemblerSupport<User, UserPageableDTO> {

  public UserAssembler() {
    super(UserController.class, UserPageableDTO.class);
  }

  @Override
  public UserPageableDTO toModel(User entity) {
    UserPageableDTO user = instantiateModel(entity);
    user.add(linkTo(methodOn(UserController.class).getOrderById(entity.getId())).withSelfRel());
    user.id = entity.getId();
    user.name = entity.getName();
    user.cpf = entity.getCpf();
    user.age = entity.getAge();
    return user;
  }

  @Override
  public CollectionModel<UserPageableDTO> toCollectionModel(Iterable<? extends User> entities) {
    CollectionModel<UserPageableDTO> orders = super.toCollectionModel(entities);
    orders.add(linkTo(methodOn(UserController.class).getAll()).withSelfRel());

    return orders;
  }
}
