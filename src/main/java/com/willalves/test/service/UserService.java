package com.willalves.test.service;

import com.querydsl.core.BooleanBuilder;
import com.willalves.test.domain.User;
import com.willalves.test.dto.UserDTO;
import com.willalves.test.exception.NotFoundException;
import com.willalves.test.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class UserService {

  private final UserRepository userRepository;

  public List<User> findAll() {
    return (List<User>) userRepository.findAll();
  }

  public Page<User> findAll(Pageable pageable, BooleanBuilder booleanBuilder) {
    return userRepository.findAll(booleanBuilder, pageable);
  }

  public User save(User user) {
    return userRepository.save(user);
  }

  public void delete(String id) {
    userRepository.delete(getById(id));
  }

  public User getById(String id) {
    return userRepository.findById(id).orElseThrow(NotFoundException::new);
  }

  public User putUpdate(String id, UserDTO updatedUser) {
    User user = getById(id);

    user.setCpf(updatedUser.getCpf());
    user.setName(updatedUser.getName());
    user.setBirthDate(updatedUser.getBirthDate());
    return save(user);
  }

  public User patchUpdate(String id, UserDTO updatedUser) {
    User user = getById(id);

    if (updatedUser.getName().isBlank() || updatedUser.getName() != null)
      user.setName(updatedUser.getName());
    if (updatedUser.getName().isBlank() || updatedUser.getCpf() != null)
      user.setCpf(updatedUser.getCpf());
    if (updatedUser.getName().isBlank() || updatedUser.getBirthDate() != null)
      user.setBirthDate(updatedUser.getBirthDate());
    return save(user);
  }
}
