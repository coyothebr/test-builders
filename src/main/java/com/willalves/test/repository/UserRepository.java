package com.willalves.test.repository;

import com.willalves.test.domain.User;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, String>, QuerydslPredicateExecutor<User> {
}
