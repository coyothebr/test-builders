package com.willalves.test.repository.specification;

import com.querydsl.core.types.Predicate;
import com.willalves.test.domain.QUser;

public class CustomUserSpecification {

  public static Predicate name(String name) {
    return QUser.user.name.eq(name);
  }
  public static com.querydsl.core.types.Predicate cpf(String cpf) {
    return QUser.user.cpf.eq(cpf);
  }

}
