package com.willalves.test.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.querydsl.core.annotations.QueryEntity;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.beans.Transient;
import java.time.LocalDate;
import java.time.Period;

import java.util.UUID;

@QueryEntity
@Data
@Document
public class User {
    private String id = UUID.randomUUID().toString();
    private String name;
    private String cpf;

    @JsonFormat(pattern="dd/MM/yyyy")
    private LocalDate birthDate;

    @Transient
    public Integer getAge(){
        Period diff= Period.between(this.birthDate, LocalDate.now());
        return diff.getYears();
    }
}
