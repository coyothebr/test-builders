package com.willalves.test.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.willalves.test.domain.User;
import lombok.Data;

import java.time.LocalDate;
import java.util.Date;


@Data
public class UserDTO {
    private String name;
    private String cpf;

    @JsonFormat(pattern="dd/MM/yyyy")
    private LocalDate birthDate;

    public User toDomain(){
        User user = new User();
        user.setName(this.name);
        user.setBirthDate(this.birthDate);
        user.setCpf(this.cpf);
        return user;
    }
}
