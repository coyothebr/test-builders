package com.willalves.test.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.springframework.hateoas.RepresentationModel;

import java.time.LocalDate;


@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserPageableDTO extends RepresentationModel<UserPageableDTO> {

    public String id;
    public String name;
    public String cpf;
    @JsonFormat(pattern="dd/MM/yyyy")
    public LocalDate birtDate;
    public Integer age;
}
