package com.willalves.test.exception;

public class NotFoundException extends RuntimeException {


    public NotFoundException() {
        super("Not Found");
    }

    public NotFoundException(String s) {
        super(s);
    }
}
